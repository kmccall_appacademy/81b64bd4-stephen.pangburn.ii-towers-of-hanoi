# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    puts "\nTower of Hanoi v.0.0.1\n-------------------\n"
    render
    loop do
      print "Choose a pile to take a disc from: "
      from_tower = get_valid_input
      print "Choose a new pile to place the disc on: "
      to_tower = get_valid_input

      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
        puts "\nMoved disc from Tower #{from_tower} to Tower #{to_tower}.\n"
        render

        if won?
          puts "You won! (ﾉ´ヮ´)ﾉ*:･ﾟ✧\n\n"
          break
        end
      else
        puts "\nThat move isn't allowed. Please try again.\n\n"
      end
    end
  end

  def render
    # Minimally Playable Edition (tm)
    @towers.each_with_index do |tower, i|
      print "#{i}: "
      tower.each do |disc|
        print "#{disc} "
      end
      puts
    end
    puts
  end

  def move(from_tower, to_tower)
    @towers[to_tower].push(@towers[from_tower].pop)
  end

  def valid_move?(from_tower, to_tower)
    return false if @towers[from_tower].empty? || from_tower == to_tower
    return true if @towers[to_tower].empty?
    @towers[from_tower].last < @towers[to_tower].last
  end

  def get_valid_input
    loop do
      input = gets.chomp.to_i
      input
      if (0..2).include?(input)
        return input
      else
        print "Invalid tower index. Please try again: "
      end
    end
  end

  def won?
    @towers[1].length == 3 || towers[2].length == 3
  end
end

if __FILE__ == $PROGRAM_NAME
  TowersOfHanoi.new.play
end
